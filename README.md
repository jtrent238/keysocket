﻿#### This is a fork of the original [borismus/keysocket](https://github.com/borismus/keysocket)

There are no major changes, but since the old version is no longer maintained I will keep this extension updated for new websites. Make a pull request to add your own sites.


Global keyboard bindings to control your Chrome-based music player. Allows your keyboard media keys (play/pause, next, previous) to work when you're listening to music on common streaming websites.

**UPDATE: Now uses Chrome's built in key binding (as of version 25)**

**Supported sites:**
   * Built in/HTML5 Chrome audio/video player
   * Amazon music player
   * Bop
   * Deezer
   * Digitally Imported (di.fm)
   * Gaana.com
   * Google Music
   * Groove Music
   * Hype Machine
   * Jamstash
   * Jango.com
   * JB Hi-Fi Now
   * Livestream.com
   * Music Choice
   * Naxos Music Library
   * Ok.ru
   * Overcast
   * Pandora
   * Phish Tracks
   * Plex
   * Pocketcasts.com
   * Rdio
   * Relax-Hub.com
   * Saavn.com
   * Slacker
   * SomaFM
   * Songza
   * SoundCloud
   * Spotify
   * Superplayer.fm
   * Synology Audio Station v.5
   * thesixtyone
   * Tidal
   * Tracksflow.com
   * Twitch.tv
   * Ustream.tv
   * vk.com (Vkontakte)
   * Xiami Music
   * Youtube.com
   * Zvooq
   * Яндекс.Музыка (Yandex.Music)
   * Яндекс.Радио (Yandex.Radio)

# Usage

1. Install extension from the [chrome web store][crx]. (This fork is not on the webstore yet, that is a link to the old version.)
2. Edit the `Keyboard shortcuts` to give Keysocket 'Global' permissions
    * open a browser tab to [chrome://extensions](chrome://extensions)
    * scroll to the bottom & click `Keyboard shortcuts`
    * find `Keysocket Media Keys` and change each desired key to `Global`

# Contribute please!

* Looking for adapters for other music players.

[crx]: https://chrome.google.com/webstore/detail/fphfgdknbpakeedbaenojjdcdoajihik
